Author Darya Sinitsa
variant 7

Task:
Project “Clock”. An adapter must be implemented in the project, which makes it possible to use a clock with arrows in the same way as a digital clock. 
In the class “Clock with hands” the turns of the hands are stored.

The project was created for passing on the EPAM Systems company courses.

The project used jdk 11.0.1, Junit 4.12, log4j 1.2.17 and Apache Maven.

Run ClockPanel
