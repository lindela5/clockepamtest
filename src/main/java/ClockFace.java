import adapter.ClockNumberAdapter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;

public class ClockFace extends JPanel {

    public ClockFace() {
        this.startClock();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(600, 600);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g.create();

        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, 600, 600);
        g2d.setColor(Color.WHITE);
        g2d.translate(300, 300);

        //Drawing the hour markers
        for (int i = 0; i < 12; i++) {

            g2d.drawLine(0, -260, 0, -300);
            g2d.rotate(Math.PI / 6);

        }

        final ClockNumberAdapter clockNumberAdapter = new ClockNumberAdapter(g2d);
        LocalTime now = LocalTime.now();
        clockNumberAdapter.toArrowClock(now);
        g2d.dispose();
    }

    public void startClock() {
        Timer timer = new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        });
        timer.start();
    }

}