package adapter;

import java.awt.*;
import java.time.LocalTime;

import number.ClockNumber;
import arrow.ClockArrow;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class ClockNumberAdapter {

    private static final Logger logger = LogManager.getLogger(ClockNumberAdapter.class);

    private final Graphics2D graphics2D;

    public ClockNumberAdapter(Graphics2D graphics2D) {
        this.graphics2D = graphics2D;
    }

    public void toArrowClock(LocalTime now) {
        final ClockNumber clockNumber = new ClockNumber(now.getSecond(), now.getMinute(), now.getHour());
        drawArrowClock(new ClockArrow(clockNumber));
    }

    private void drawArrowClock(ClockArrow clockArrow) {
        logger.info("Second angle: " + clockArrow.getAngleSeconds());
        graphics2D.rotate(clockArrow.getAngleSeconds());
        graphics2D.drawLine(0, 0, 0, -290);

        logger.info("Minute angle: " + clockArrow.getAngleMinutes());
        graphics2D.rotate(2 * Math.PI - clockArrow.getAngleSeconds());
        graphics2D.rotate(clockArrow.getAngleMinutes());
        graphics2D.setStroke(new BasicStroke(3));
        graphics2D.drawLine(0, 0, 0, -250);

        logger.info("Hour angle: " + clockArrow.getAngleHours());
        graphics2D.rotate(2 * Math.PI - clockArrow.getAngleMinutes());
        graphics2D.rotate(clockArrow.getAngleHours());
        graphics2D.setStroke(new BasicStroke(6));
        graphics2D.drawLine(0, 0, 0, -200);
    }
}

