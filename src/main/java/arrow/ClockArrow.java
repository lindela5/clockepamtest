package arrow;

import number.ClockNumber;

public class ClockArrow {
    private ClockNumber clock;

    public ClockArrow(ClockNumber clock) {
        this.clock = clock;
    }

    public double getAngleSeconds() {
        double result;
        result = clock.getSeconds() * Math.PI / 30;
        return result;
    }

    public double getAngleMinutes() {
        double result;
        result = clock.getMinutes() * Math.PI / 30;
        return result;
    }

    public double getAngleHours() {
        double result;
        result = clock.getHours() * Math.PI / 6;
        return result;
    }

}