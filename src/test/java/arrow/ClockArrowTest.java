package arrow;

import number.ClockNumber;
import org.junit.Test;

import static org.junit.Assert.*;

public class ClockArrowTest {

    private ClockArrow clockArrow;

    @Test
    public void shouldGetRightAngleSeconds() {
        final ClockNumber clock = new ClockNumber(12, 10, 10);
        clockArrow = new ClockArrow(clock);
        assertEquals(1.2566370614359172, clockArrow.getAngleSeconds(), 0.001);
    }

    @Test
    public void getAngleMinutes() {
        final ClockNumber clock = new ClockNumber(20, 30, 3);
        clockArrow = new ClockArrow(clock);
        assertEquals(3.1415926535897927, clockArrow.getAngleMinutes(), 0.001);
    }

    @Test
    public void getAngleHours() {
        final ClockNumber clock = new ClockNumber(7, 15, 7);
        clockArrow = new ClockArrow(clock);
        assertEquals(3.665191429188092, clockArrow.getAngleHours(), 0.001);
    }
}